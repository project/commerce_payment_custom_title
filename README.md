# Commerce Payment Custom Title

This module allows admin to customize display title of commerce payment methods.

When editing a payment method rules action, you can set the display title (also could use html markup).

## Compatibility

This module work by replaces checkout_form callback of payment method pane (commerce_payment_pane_checkout_form) and execute callback of payment method enable rules action (commerce_payment_enable_method).
Any other modules replaces same callback could be broken.

Also, some modules overwrites display_title by form_alter hooks.
(eg. commerce_paypal and sub modules)

## Installation

Standard module installation applies. See
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

## Configuration

When edit payment method action settings, you can configure Display Title.

## Translation

You could translate a display title with rules i18n.
