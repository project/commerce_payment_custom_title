<?php
/**
 * @file
 * Rules hooks
 */

/**
 * Implements hook_rules_action_info_alter().
 */
function commerce_payment_custom_title_rules_action_info_alter(&$actions) {
  foreach (commerce_payment_methods() as $payment_method) {
    $action = &$actions['commerce_payment_enable_' . $payment_method['method_id']];
    $action['parameter']['display_title'] = array(
      'type' => 'text',
      'label' => t('Display title'),
      'optional' => TRUE,
      'translatable' => TRUE,
    );
    $action['callbacks']['execute'] = 'commerce_payment_custom_title_enable_method';
  }
}
